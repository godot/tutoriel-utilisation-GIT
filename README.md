Les commandes principales à utiliser sur votre Git local pour travailler en lien avec framagit :
ces commandes sont à taper dans votre terminal en local

tutoriel plus détaillé sur [https://godot.tuxfamily.org/tutoriels/utilisation-de-git] 


# Git global setup

configurer votre git local : voir [https://docs.framasoft.org/fr/gitlab/] 
    

# Copier en local un dépot framagit pour pouvoir ensuite travailler collaborativement

git clone git@framagit.org:godot/ressources-decors-3D.git   (ou le nom du répertoire concerné)

cd ressources-decors-3D

git add .    ou git add *       (pour ajouter la ou les modifications (ou creations de fichier) sur votre Git local)

git commit -m "add README par exemple ou tout autre commentaire expliquant le contenu des modifications"   (pour grouper toutes vos modifications en un "commit", pret à être transmis)

git push origin master  (pour pousser votre "commit" sur le framagit distant, dans la branche master...)

